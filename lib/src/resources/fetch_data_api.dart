import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:list_users_bloc/src/models/user_model.dart';

class FetchDataFromApi {
  Future<UserModel> fetchUser() async {
    final http.Response response =
        await http.get(Uri.parse("https://reqres.in/api/users?page=2"));
    //         Uri.parse
    // To improve compile-time type safety, package:http 0.13.0 introduced breaking changes
    // that made all functions that previously accepted Uris or Strings
    // now accept only Uris instead.
    // You will need to use Uri.parse to convert a String into a Uri:

    if (response.statusCode == 200) {
      return UserModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load');
    }
  }
}
