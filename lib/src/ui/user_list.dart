import 'package:flutter/material.dart';
import 'package:list_users_bloc/src/blocs/user_bloc.dart';
import 'package:list_users_bloc/src/models/user_model.dart';
import 'package:provider/provider.dart';

class UserList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<UserBloc>(context);
    return Scaffold(
      body: StreamBuilder(
        stream: bloc.userStream,
        builder: (BuildContext context, AsyncSnapshot<UserModel> snapshot) {
          if (snapshot.hasData) {
            List<Datum> listOfUsersInfo = snapshot.data.userInfo;
            return ListView.builder(
              itemCount: listOfUsersInfo.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(
                  child: ListTile(
                    leading: Image.network(listOfUsersInfo[index].avatar),
                    title: Text(
                      '${listOfUsersInfo[index].id.toString()} ${listOfUsersInfo[index].firstName} ${listOfUsersInfo[index].lastName}',
                    ),
                    subtitle: Text('email: ${listOfUsersInfo[index].email}'),
                  ),
                );
              },
            );
          }
          if (snapshot.hasError) {
            return Text(snapshot.error);
          }
          return CircularProgressIndicator();
        },
      ),
    );
  }
}
