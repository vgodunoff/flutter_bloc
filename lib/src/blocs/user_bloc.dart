import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:list_users_bloc/src/models/user_model.dart';
import 'package:list_users_bloc/src/resources/fetch_data_api.dart';

class UserBloc {
  final _userController = StreamController<UserModel>();
  Stream<UserModel> get userStream => _userController.stream;
  Sink<UserModel> get addUser => _userController.sink;
  final fetchDataFromApi = FetchDataFromApi();
  fetchUsersToStream() async {
    UserModel user = await fetchDataFromApi.fetchUser();
    addUser.add(user);
  }

  UserBloc() {
    fetchUsersToStream();
  }
  dispose() {
    _userController.close();
  }
}
