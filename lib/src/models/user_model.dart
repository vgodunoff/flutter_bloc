import 'dart:convert';

UserModel userFromJson(String str) => UserModel.fromJson(json.decode(str));

class UserModel {
  UserModel({
    this.userInfo,
  });

  List<Datum> userInfo;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        userInfo: List<Datum>.from(
            json["data"].map((element) => Datum.fromJson(element))),
      );
}

class Datum {
  Datum({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.avatar,
  });

  int id;
  String email;
  String firstName;
  String lastName;
  String avatar;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        avatar: json["avatar"],
      );
}
